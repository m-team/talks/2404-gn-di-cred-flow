---
# vim:tw=100:ft=markdown
author: Marcus Hardt, Wolfgang Pempe

title: <smaller>Interworking Architecture</smaller>
<!--Integration of Infrastructure Capacity in EOSC: policy & technical overview-->
date: Apr 2024
theme: marcus2
parallaxBackgroundImage: images/kit-bg-slide.png
title-slide-attributes:
    data-background-image: images/kit-bg-head.png
slideNumber: \'c/t\'
preloadIframes: true
pdfSeparateFragments: false
pdfMaxPagesPerSlide: 1
showNotes: false
mouseWheel: true
transition: none
<!--backgroundTransition: none-->

---

## ToIP Trust
<img style="vertical-align:middle" src="images/toip-trust.png"/ height="500">

## ToIP Trust
<img style="vertical-align:middle" src="images/trust-diamond.png"/ height="500">

## ToIP Layers
<img style="vertical-align:middle" src="images/four-layers.png"/ height="500">

## ToIP Layers
<img style="vertical-align:middle" src="images/four-layers-explained.png"/ height="500">


## Title 
<img style="vertical-align:middle" src="images/ssi-federation.png"/ height="500">

## Title 
<img style="vertical-align:middle" src="images/saml-federation.png"/ height="500">


## IAM General Flow 
<img style="vertical-align:middle" src="images/general-flow.png"/ height="500">

## IAM General Data Flow 
<img style="vertical-align:middle" src="images/flow-with-data.png"/ height="500">

## IAM General Trust Flow
<img style="vertical-align:middle" src="images/flow-with-trust.png"/ height="500">


# json
## `sd-jwt`

- Issuer sends `sd-jwt` and `sd-jwt-svc` to holder
    - `sd-jwt`: 
        - claims and hashes (no values)
        - signed
    - `sd-jwt-svc`:
        - salt value container
        - use salt with crypto to verify hash
- Holder relaeses `sd-jwt-r`
    - R for reduced
    - subset of `sd-jwt-svg`
    - Holder can see actual values released
- Verifier obtains `sd-jwt` and `sd-jwt-r`
    - Verify hashes
    - Trust values


## `json-ld`

- JSON with Linked Data
- `@context` points to a descriptive schema of the json
- Several `@` extensions
- Allows very flexible support of arbitrary `jsons`


# `OID-Fed`
## `OID-Fed`

- Framework to model trust
    - Two Dimensions (2D): Trust anchors + Trustmarks
- Transition from `eduGAIN` in developmenat
